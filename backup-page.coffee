# Copyright (c) 2013 Michele Bini

# This program is free software: you can redistribute it and/or modify
# it under the terms of the version 3 of the GNU General Public License
# as published by the Free Software Foundation.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

serverBase = "SERVERBASE"
data = document.body.innerHTML
req = new XMLHttpRequest
req.open "PUT", serverBase, true
msg = document.createElement "div"
msg.setAttribute "style", """
position:absolute;
z-index:200000000;
width:100%;
text-align:center;
font-size:150%;
background:blue;
color:white;
cursor:pointer;
"""

msg.setAttribute "onclick", "javascript:this.parentNode.removeChild(this)"
msg.innerHTML = "..."
prepend = (x, y) -> x.parentNode.insertBefore(y, x)
prepend document.body.firstChild, msg
del = (x) -> x.parentNode.removeChild(x)
closeBtn = "<span style=\"cursor:pointer\" onclick=\"javascript:this.parentNode.parentNode.removeChild(this.parentNode)\">[×]</span>"
saveDocument = "Save this document"
req.onreadystatechange = ->
  if @readyState is 4
    if @status is 201
      console.log 'Page saved'
      msg.innerHTML = "<span>Saved</span> #{closeBtn}"
      setTimeout (-> del msg), 2000
      return
    else
      msg.innerHTML = "<span>Saving... <script src=\"#{serverBase}#{encodeURIComponent(data)}\">#{saveDocument}</a></script> #{closeBtn}"

req.send data
