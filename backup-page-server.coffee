# Copyright (c) 2013 Michele Bini

# This program is free software: you can redistribute it and/or modify
# it under the terms of the version 3 of the GNU General Public License
# as published by the Free Software Foundation.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


http    = require 'http'
fs      = require 'fs'
crypto  = require 'crypto'
urlLib  = require 'url'

port = 8414

hash = (data) -> crypto.createHash('md5').update(data).digest("hex")

n = 0

readAll = (stream, thunk) ->
  chunks = []
  stream.on 'data', (data) -> chunks.push data
  stream.on 'end', () -> thunk chunks.join("")

withAsyncData = (obtain) -> do (obtaining = false, clients = [], obtained = false, error = false, result = false) -> (f) ->
  return f null, result if obtained
  return f error if error
  clients.push f
  unless obtaining
    obtaining = 1
    try
      obtain (err, r) ->
        if err
          error = err
          f error for f in clients
        else
          result = r
          obtained = 1
          f null, result for f in clients
    catch e
      error = e
      f error for f in clients

# Compile coffee source into bookmarklet
coffeemarklet = do ->
  { compile } = require 'coffee-script'
  minify = do ->
    UGL = require 'uglify-js2'
    (code) ->
      ast = UGL.parse code
      ast.figure_out_scope()
      ast = ast.transform(UGL.Compressor())
      ast.figure_out_scope()
      ast.print_to_string().replace(/;$/, "")
  (code) -> "javascript:" + encodeURIComponent minify compile code

withBookmarklet = do ->
  withAsyncData (give) ->
    fs.readFile "backup-page.coffee", (err, data) ->
      return give err if err
      give(null, coffeemarklet "#{data}")

# withBookmarklet (err, b) ->
#   console.log "Gotten bookmarklet"
#   throw err if err
#   console.log b

runServer = -> http.createServer((req, res) ->
  mkname = (name) ->
    name = name.replace /[^0-9a-zA-Z.]+/g, "-"
    name = name.replace /^-+/,   ""
    name = name.replace /-+$/,  ""
    if name.length > 44
      name = name.substring(0,20) + '-..-' + name.substring(0,20)
    name
  reply = (httpStatus = 200, msg) ->
    res.writeHead httpStatus,
      'Content-Type': 'text/javascript'
      'Access-Control-Allow-Origin':   '*'
    res.end msg
    log "Completed"
  errorReply =  (msg, httpStatus = 400) ->
    log msg
    msg.replace(/"/g, "\\\"") # "
    res.writeHead httpStatus,
      'Content-Type':                 'text/plain'
      'Access-Control-Allow-Origin':  '*'
    res.end msg
    log "Completed with error"
  internalError = (msg) -> errorReply "Internal error: #{msg}", 500
  documentBookmarklet = ->
    withBookmarklet (err, bookmarklet) ->
      return internalError "could not compile bookmarklet" if err
      res.writeHead 200,
        'Content-Type': 'text/html'
      serverBase = req.headers.host
      serverBase = "localhost" unless serverBase?
      serverBase = encodeURIComponent "http://#{serverBase}/"
      bookmarklet = bookmarklet.replace "SERVERBASE", serverBase
      pageTitle = 'Backup pages'
      pageBody = "Bookmarklet: <a href=\"#{bookmarklet}\">BackMeUp</a>"
      res.end "<html><head><title>#{pageTitle}</title></head><body>#{pageBody}</body></html>"
      log "Documented bookmarklet"
  store = (name, data) ->
    h = hash data
    name = if name? then "#{h}-#{name}" else h
    fs.writeFile name, data, (err) ->
      throw err if err
      log "Data saved to #{name}"
  storedata = (data) ->
    store name, data
    reply 201, 'this.parentNode.parentNode.removeChild(this.parentNode)'
  log = (msg) -> console.log "Connection #{connn}: #{msg}"
  peek = (obj) -> log "Peek #{obj}"; obj
  connn = ++n
  url = req.url
  method = req.method
  query = urlLib.parse(url, true).query
  log "#{method} #{url}"
  if method is 'OPTIONS'
    allowOrigin = ->
      return '*'
      if (origin = req.headers.origin)?
        return origin
      if (referer = req.headers.referer)?
        { protocol, hostname, port } = urlLib.parse referer
        return urlLib.format { hostname, port }
      '*'
    allowHeaders = ->
      x = req.headers['access-control-request-headers']
      if x? then x = x.split(/, */) else x = []
      (x.push y if not (y in x)) for y in [ 'origin', 'content-type' ]
      x.join(', ')
    res.writeHead 200,
      'Access-Control-Allow-Origin':   peek do allowOrigin
      'Access-Control-Allow-Methods':  'POST, PUT, GET, OPTIONS'
      'Access-Control-Allow-Headers':  peek do allowHeaders
    res.end ''
    log "Replied with policy"
  else
    name = mkname name if (name = query.name)? or (name = req.headers.referer)?
    if (data = query.data)?
      storedata data
    else if method is 'PUT'
      readAll req, storedata
    else if method is 'GET'
      do documentBookmarklet
    else
      errorReply "No data provided"
).on('error', (error) -> console.log "Error: " + error.code).listen(port)

try
  do runServer
  console.log "Webpage backup server running at port #{port}"
  console.log "Now you can browse to http://localhost:#{port}/,"
  console.log "get your bookmarklet and start backing pages up!"
catch error
  console.log "Error starting server: #{error}"
